package com.example.otimus.paaniaayo.rest;

import com.example.otimus.paaniaayo.model.Photos;
import com.example.otimus.paaniaayo.model.PlacesItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Otimus on 2/1/2017.
 */

public interface ApiInterface {

    @GET("getplaces.php")
    Call<List<PlacesItem>> getPlaces();

    @GET("/api/getphotos.php")
    Call<List<Photos>> getPhotos(@Query("place_id") Integer place_id);


}


